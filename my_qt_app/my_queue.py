import socket
import struct
from threading import Thread
import time
from datetime import datetime

from PyQt5.QtCore import QObject, pyqtSignal

import traceback

import str_to_byte

import queue

# ~ x   байт набивки    нет значения
# ~ c   char    bytes длины 1   1
# ~ b   signed char integer 1
# ~ B   unsigned char   integer 1
# ~ ?   _Bool   bool    1
# ~ h   short   integer 2
# ~ H   unsigned short  integer 2
# ~ i   int integer 4
# ~ I   unsigned int    integer 4
# ~ l   long    integer 4
# ~ L   unsigned long   integer 4
# ~ q   long long   integer 8
# ~ Q   unsigned long long  integer 8
# ~ n   ssize_t integer зависит
# ~ N   size_t  integer зависит
# ~ e   «половинный float«  float   2
# ~ f   float   float   4
# ~ d   double  float   8
# ~ s   char[]  bytes   указывается явно
# ~ p   char[] — строка из Паскаля  bytes   указывается явно

# ~ >>> struct.pack("<h", 258)  # little-endian
# ~ b'\x02\x01'
# ~ >>> struct.pack(">h", 258)  # big-endian
# ~ b'\x01\x02'

def str_of_bytes(bytes_str):
    try:
        return r"\x" + r"\x".join("{:02x}".format(c) for c in bytes_str)
    except:
        try:
            return r"\x" + "{:02x}".format(bytes_str)
        except:
            print('ERROR: input string is not "bytes" type')
            return ""

class Queue(QObject):

    message_received = pyqtSignal(bytes)
    message_for_send = pyqtSignal(bytes)
    register_w_changed = pyqtSignal(str)
    register_r_changed = pyqtSignal(str)
    data_received = pyqtSignal(bytes)

    def __init__(self, parent=None):
        super(Queue, self).__init__(parent)

        self.commands_queue = queue.Queue()

        self.connect_flag = False
        self.run_flag = False
        self.meas_flag = False

        self.th_sender = Thread(target = self.sender, args = (self.commands_queue,))
        self.th_sender.start()

    def sender(self, queue):
        while 1:
            message = queue.get()
            print("queue: send:", str_of_bytes(message))
            self.message_for_send.emit(message)

    def connection(self, flag):
        self.connect_flag = flag
        print("connected", flag)

    def ping(self):
        self.run_flag = False
        tmp = b'\xff' + b'\x00\x00\x00\x00\x00'
        print("ping")
        print("queue: send:", str_of_bytes(tmp))
        self.message_for_send.emit(tmp)

    def receive_mess(self, mess):
        if str_of_bytes(mess[0]) == r"\x" + "10":
            if str_of_bytes(mess[-1]) != r"\x" + "10":
                self.run_flag = True

        if str_of_bytes(mess[0]) == r"\x" + "f4":
            self.run_flag = True
            self.register_r_changed.emit(str_of_bytes(mess))


        print("queue: receive:", str_of_bytes(mess))
    # #def send_mess(self, mess):
        # #if str_of_bytes(data[0]) == r"\x" + "10":


    def change_reg(self, name, value):
        command = b'\x0c'
        if name == "Turns number:":
            reg_address_1 = b'\x01'
            reg_address_2 = b'\x02'
            N = struct.pack('>L', int(value))
            mess1 = command + reg_address_1 + N[2:]
            mess2 = command + reg_address_2 + N[:2]
            # #print("mess1", str_of_bytes(mess1))
            # #print("mess2", str_of_bytes(mess2))
            self.commands_queue.put(mess1)
            self.commands_queue.put(mess2)

            # #print(str_of_bytes(mess1))
            # #print(str_of_bytes(mess2))
            self.register_w_changed.emit(str_of_bytes(mess1))
            self.register_w_changed.emit(str_of_bytes(mess2))

        if name == "Separatrix:":
            reg_address = b'\x06'
            tmp = struct.pack('>B', 255 - int(value))
            mess = command + reg_address + b'\x00' + tmp
            self.commands_queue.put(mess)

            # #print(str_of_bytes(mess))
            self.register_w_changed.emit(str_of_bytes(mess))

        if name == "Fine delay (ps):":
            reg_address = b'\x08'
            tmp = struct.pack('>H', int(value/10))
            mess = command + reg_address + tmp
            self.commands_queue.put(mess)

            # #print(str_of_bytes(mess))
            self.register_w_changed.emit(str_of_bytes(mess))

        if name == "Fronts overlay:":
            reg_address = b'\x04'
            tmp = struct.pack('>H', int(value))
            mess = command + reg_address + tmp
            self.commands_queue.put(mess)

            # #print(str_of_bytes(mess))
            self.register_w_changed.emit(str_of_bytes(mess))

        if name == "ADC delay:":
            reg_address = b'\x09'
            tmp = struct.pack('>H', int(value))
            mess = command + reg_address + tmp
            self.commands_queue.put(mess)

            # #print(str_of_bytes(mess))
            self.register_w_changed.emit(str_of_bytes(mess))

    def change_reg_spec(self, gain, trigger):
        command = b'\x0c'
        reg_address = b'\x00'
        text = "%d00000000000000%d" % (trigger, gain)
        reg_value = struct.pack('>H', int(text, 2))
        mess = command + reg_address + reg_value
        self.commands_queue.put(mess)

        # #print(str_of_bytes(mess))
        self.register_w_changed.emit(str_of_bytes(mess))









