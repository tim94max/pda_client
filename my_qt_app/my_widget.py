from PyQt5.QtCore import QDateTime, Qt, QTimer, pyqtSignal, QCoreApplication
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
        QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
        QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
        QVBoxLayout, QWidget)

from my_led import LedIndicator

import str_to_byte

import os
import sys

def str_of_bytes(bytes_str):
    try:
        return r"\x" + r"\x".join("{:02x}".format(c) for c in bytes_str)
    except:
        try:
            return r"\x" + "{:02x}".format(bytes_str)
        except:
            print('ERROR: input string is not "bytes" type')
            return ""

class MainWindow(QDialog):

    UDP_PORT = 2195
    try_connect = pyqtSignal(str, int)

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # #self.originalPalette = QApplication.palette()
        # #QApplication.style().standardPalette()
        QApplication.setStyle(QStyleFactory.create("QtCurve"))

        self.createNetGroupbox()
        self.createConfigGroupbox()
        self.createRegistersGroupbox()

        mainLayout = QGridLayout()
        mainLayout.addWidget(self.netGroupBox, 0, 0)
        mainLayout.addWidget(self.configGroupBox, 0, 1)
        mainLayout.addWidget(self.registersGroupbox, 0, 2)


        self.exit_BP = QPushButton("EXIT")
        mainLayout.addWidget(self.exit_BP, 1, 2)
        self.exit_BP.clicked.connect(self.quit_app)

        self.setLayout(mainLayout)

        self.IPadress = self.IP_item.text()

        self.configGroupBox.setEnabled(False)
        self.registersGroupbox.setEnabled(False)

    def quit_app(self):
        QApplication.closeAllWindows()
        QApplication.instance().quit()
        # #self.close()
        # #QApplication.quit()
        # #exit()
        sys.exit()
        # #quit()
        # #self.termination()

    def reg_w(self, data):
        if data[:4] == r"\x" + '0c' or data[0:4] == r"\x" + '00':
            if data[4:8] == r"\x" + "00":
                self.reg0_write.setText(data[8:])

            elif data[4:8] == r"\x" + "01":
                self.reg1_write.setText(data[8:])

            elif data[4:8] == r"\x" + "02":
                self.reg2_write.setText(data[8:])

            elif data[4:8] == r"\x" + "04":
                self.reg4_write.setText(data[8:])

            elif data[4:8] == r"\x" + "06":
                self.reg6_write.setText(data[8:])

            elif data[4:8] == r"\x" + "08":
                self.reg8_write.setText(data[8:])

            elif data[4:8] == r"\x" + "09":
                self.reg9_write.setText(data[8:])

            else:
                print("WARNING: There is not this register number:", data[4:8])
        else:
            print(("WARNING: This command isn't 'write register':", data[:4]))

    def reg_r(self, data):
        if data[:4] == r"\x" + 'f4':
            if data[4:8] == r"\x" + "00":
                self.reg0_read.setText(data[8:])

            elif data[4:8] == r"\x" + "01":
                self.reg1_read.setText(data[8:])

            elif data[4:8] == r"\x" + "02":
                self.reg2_read.setText(data[8:])

            elif data[4:8] == r"\x" + "04":
                self.reg4_read.setText(data[8:])

            elif data[4:8] == r"\x" + "06":
                self.reg6_read.setText(data[8:])

            elif data[4:8] == r"\x" + "08":
                self.reg8_read.setText(data[8:])

            elif data[4:8] == r"\x" + "09":
                self.reg9_read.setText(data[8:])

            else:
                print("WARNING: There is not this register number:", data[4:8])
        else:
            print(("WARNING: This command isn't 'write register':", data[:4]))

    def change_status(self, status):
        print("main_window: connection status", status)
        self.configGroupBox.setEnabled(status)
        self.registersGroupbox.setEnabled(status)
        self.status_LED.setEnabled(status)


    def createNetGroupbox(self):
        self.netGroupBox = QGroupBox("Net")

        IP_Label = QLabel("IP adress:")
        self.IP_item = QLineEdit("172.16.1.166")

        self.connect_PB = QPushButton("Connect")

        self.config_PB = QPushButton("Configure PDA")

        self.disconnect_PB = QPushButton("Disconnect")

        # #self.ping_std_PB = QPushButton("Ping (standart)")
        self.ping_my_PB = QPushButton("Ping (custom)")

        status_label = QLabel("Status:")
        self.status_LED = LedIndicator()
        self.status_LED.setEnabled(False)

        state_label = QLabel("Clone state (after connection):")
        self.state_json = QRadioButton("Use current local state")
        self.state_PDA = QRadioButton("Load PDA state")
        self.state_json.setChecked(True)

        layout = QVBoxLayout()

        layout_1 = QHBoxLayout()
        layout_1.addWidget(IP_Label)
        layout_1.addWidget(self.IP_item)
        layout.addLayout(layout_1)

        layout.addWidget(self.connect_PB)
        layout.addWidget(self.config_PB)
        layout.addWidget(self.disconnect_PB)
        layout.addWidget(self.ping_my_PB)

        layout.addWidget(state_label)
        layout.addWidget(self.state_json)
        layout.addWidget(self.state_PDA)

        layout_2 = QHBoxLayout()
        layout_2.addWidget(status_label)
        layout_2.addWidget(self.status_LED)
        layout.addLayout(layout_2)

        layout.addStretch(0)

        self.netGroupBox.setLayout(layout)


    def createConfigGroupbox(self):
        self.configGroupBox = QGroupBox("Confuguration")

        Nc_label = QLabel("Turns number:")
        self.Nc_spinbox = QSpinBox()
        self.Nc_spinbox.setRange(0, 2730)
        self.Nc_spinbox.setSingleStep(1)

        sep_label = QLabel("Separatrix:")
        self.sep_spinbox = QSpinBox()
        self.sep_spinbox.setRange(0, 11)
        self.sep_spinbox.setSingleStep(1)

        delay_label = QLabel("Fine delay (ps):")
        self.delay_spinbox = QSpinBox()
        self.delay_spinbox.setRange(0, 1023 * 10)
        self.delay_spinbox.setSingleStep(10)

        start_page_label = QLabel("Start memory page:")
        self.start_page_spinbox = QSpinBox()
        self.start_page_spinbox.setRange(0, 63)
        #self.start_page_spinbox.setValue(63)
        self.start_page_spinbox.setSingleStep(1)

        stop_page_label = QLabel("Stop memory page:")
        self.stop_page_spinbox = QSpinBox()
        self.stop_page_spinbox.setRange(0, 63)
        self.stop_page_spinbox.setSingleStep(1)

        gain_label = QLabel("Gain:")
        self.gain_spinbox = QSpinBox()
        self.gain_spinbox.setRange(0, 1)
        self.gain_spinbox.setSingleStep(1)

        trigger_label = QLabel("Trigger:")
        self.trigger_combobox = QComboBox()
        self.trigger_combobox.addItem("Internal")
        self.trigger_combobox.addItem("External")

        DEl0_label = QLabel("Fronts overlay:")
        self.DEl0_spinbox = QSpinBox()
        self.DEl0_spinbox.setRange(0, 7)
        self.DEl0_spinbox.setSingleStep(1)

        ADCdelay_label = QLabel("ADC delay:")
        self.ADCdelay_spinbox = QSpinBox()
        self.ADCdelay_spinbox.setValue(10)
        self.ADCdelay_spinbox.setEnabled(False)

        self.configs_arr = [[Nc_label, self.Nc_spinbox],
            [sep_label, self.sep_spinbox],
            [delay_label, self.delay_spinbox],
            [start_page_label, self.start_page_spinbox],
            [stop_page_label, self.stop_page_spinbox],
            [gain_label, self.gain_spinbox],
            [trigger_label, self.trigger_combobox],
            [DEl0_label, self.DEl0_spinbox],
            [ADCdelay_label, self.ADCdelay_spinbox]
            ]

        # #layout = QVBoxLayout()
        # #for i in arr:
            # #layout_tmp = QHBoxLayout()
            # #layout_tmp.addWidget(i[0])
            # #layout_tmp.addWidget(i[1])
            # #layout.addLayout(layout_tmp)
        # #self.configGroupBox.setLayout(layout)

        grid = QGridLayout()
        k = 0
        for i in self.configs_arr:
            # #layout_tmp = QHBoxLayout()
            grid.addWidget(i[0], k, 0)
            grid.addWidget(i[1], k, 1)
            k += 1
        self.configGroupBox.setLayout(grid)


    def createRegistersGroupbox(self):
        self.registersGroupbox = QGroupBox("Registers")

        regs = [0, 1, 2, 4, 6, 8, 9]
        #0 - gain; 1,2 - N ciles; 4 - NDEl0; 6 - sep; 8 - fane delay

        reg0_label = QLabel("0x00:")
        self.reg0_write = QLineEdit()
        self.reg0_read = QLineEdit()

        reg1_label = QLabel("0x01:")
        self.reg1_write = QLineEdit()
        self.reg1_read = QLineEdit()

        reg2_label = QLabel("0x02:")
        self.reg2_write = QLineEdit()
        self.reg2_read = QLineEdit()

        reg4_label = QLabel("0x04:")
        self.reg4_write = QLineEdit()
        self.reg4_read = QLineEdit()

        reg6_label = QLabel("0x06:")
        self.reg6_write = QLineEdit()
        self.reg6_read = QLineEdit()

        reg8_label = QLabel("0x08:")
        self.reg8_write = QLineEdit()
        self.reg8_read = QLineEdit()

        reg9_label = QLabel("0x09:")
        self.reg9_write = QLineEdit()
        self.reg9_read = QLineEdit()

        autoWrite = QCheckBox("Autowrite")
        autoWrite.setCheckState(Qt.CheckState.Checked)

        autoRead = QCheckBox("Autoread")
        autoRead.setCheckState(Qt.CheckState.Checked)

        arr = [[reg0_label, self.reg0_write, self.reg0_read],
            [reg1_label, self.reg1_write, self.reg1_read],
            [reg2_label, self.reg2_write, self.reg2_read],
            [reg4_label, self.reg4_write, self.reg4_read],
            [reg6_label, self.reg6_write, self.reg6_read],
            [reg8_label, self.reg8_write, self.reg8_read],
            [reg9_label, self.reg9_write, self.reg9_read],
            ]

        grid = QGridLayout()

        k = 0
        grid.addWidget(QLabel("Regs for write:"), k, 1)
        grid.addWidget(QLabel("Read register:"), k, 2)
        k += 1

        for i in arr:
            # #layout_tmp = QHBoxLayout()
            grid.addWidget(i[0], k, 0)
            grid.addWidget(i[1], k, 1)
            grid.addWidget(i[2], k, 2)
            k += 1

        # #grid.addWidget(autoWrite, k, 1)
        # #grid.addWidget(autoRead, k, 2)

        grid.setColumnStretch(0, 0)
        grid.setRowStretch(0, 0)
        self.registersGroupbox.setLayout(grid)

