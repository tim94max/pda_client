from PyQt5.QtWidgets import QApplication
from my_widget import MainWindow
from my_network import Network
from my_queue import Queue
import sys

UPD_PORT = 2195



if __name__ == '__main__':

    app = QApplication(sys.argv)
    main_window = MainWindow()
    network = Network()
    queue = Queue()

    # connect main_window Net box (connecting and disconneting to network)
    main_window.connect_PB.clicked.connect(lambda x: network.connect(main_window.IPadress, UPD_PORT))
    main_window.disconnect_PB.clicked.connect(lambda x: network.disconnect())

    main_window.exit_BP.clicked.connect(lambda x: sys.exit())


    # connect connecting and disconneting from network to queue
    network.connected.connect(queue.connection)
    network.disconnected.connect(queue.connection)
    # ... and to main_window
    network.connected.connect(main_window.change_status)
    network.disconnected.connect(main_window.change_status)

    # connect main_window QSpinBoxes to queue sending command to PDA
    main_window.Nc_spinbox.valueChanged.connect(lambda x: queue.change_reg("Turns number:", main_window.Nc_spinbox.value()))
    main_window.sep_spinbox.valueChanged.connect(lambda x: queue.change_reg("Separatrix:", main_window.sep_spinbox.value()))
    main_window.delay_spinbox.valueChanged.connect(lambda x: queue.change_reg("Fine delay (ps):", main_window.delay_spinbox.value()))
    main_window.DEl0_spinbox.valueChanged.connect(lambda x: queue.change_reg("Fronts overlay:", main_window.DEl0_spinbox.value()))
    main_window.ADCdelay_spinbox.valueChanged.connect(lambda x: queue.change_reg("ADC delay:", main_window.DEl0_spinbox.value()))
    # connect main_window QSpinBoxes to queue sending command to PDA (special for Trigger and Gain)
    main_window.trigger_combobox.currentIndexChanged.connect(lambda x:
        queue.change_reg_spec(main_window.gain_spinbox.value(), main_window.trigger_combobox.currentIndex()))
    main_window.gain_spinbox.valueChanged.connect(lambda x:
        queue.change_reg_spec(main_window.gain_spinbox.value(), main_window.trigger_combobox.currentIndex()))

    # connect registers writing and reading by queue to main_window QLineEdits
    queue.register_w_changed.connect(main_window.reg_w)
    queue.register_r_changed.connect(main_window.reg_r)

    #connect network and queue message sending and receiving
    network.message_received.connect(queue.receive_mess)
    queue.message_for_send.connect(network.send_message)



    main_window.show()
    sys.exit(app.exec())
