def str_of_bytes(bytes_str):
    try:
        return r"\x" + r"\x".join("{:02x}".format(c) for c in bytes_str)
    except:
        try:
            return r"\x" + "{:02x}".format(bytes_str)
        except:
            print('ERROR: input string is not "bytes" type')
            return ""
