import socket
# #import struct
from threading import Thread
# #import time
# #from datetime import datetime

from PyQt5.QtCore import QObject, pyqtSignal

import traceback


class Network(QObject):

    message_received = pyqtSignal(bytes)
    connected = pyqtSignal(bool)
    disconnected = pyqtSignal(bool)

    def __init__(self, parent=None):
        super(Network, self).__init__(parent)

    def receiver(self, socket):
        while True:
            data, addr = socket.recvfrom(2000)
            self.message_received.emit(data)

    def connect(self, UDP_IP, UDP_PORT):
        try:
            self.UDP_IP = UDP_IP
            self.UDP_PORT = UDP_PORT
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

            # #self.th_sender = Thread(target = self.sender, args = (self.sock,))
            # #self.th_sender.start()

            self.th_receiver = Thread(target = self.receiver, args = (self.sock,))
            self.th_receiver.start()
            self.connected.emit(True)
            # #print("Network: connected")

        except Exception:
            traceback.print_exc()
            print("ERROR: connection failed")

    def disconnect(self):
        try:
            self.sock.close()
            self.disconnected.emit(False)
            # #print("Network: disconnected")
        except Exception:
            traceback.print_exc()
            print("ERROR: cocket isn't connected")

    def send_message(self, message):
        try:
            self.sock.sendto(message, (self.UDP_IP, self.UDP_PORT))
        except Exception:
            traceback.print_exc()
            print("ERROR: cocket doesn't exist")





