import socket
import struct
from threading import Thread
import time
from datetime import datetime

from pages_conv import write_in_files

LEN = 5

# ~ x   байт набивки    нет значения
# ~ c   char    bytes длины 1   1
# ~ b   signed char integer 1
# ~ B   unsigned char   integer 1
# ~ ?   _Bool   bool    1
# ~ h   short   integer 2
# ~ H   unsigned short  integer 2
# ~ i   int integer 4
# ~ I   unsigned int    integer 4
# ~ l   long    integer 4
# ~ L   unsigned long   integer 4
# ~ q   long long   integer 8
# ~ Q   unsigned long long  integer 8
# ~ n   ssize_t integer зависит
# ~ N   size_t  integer зависит
# ~ e   «половинный float«  float   2
# ~ f   float   float   4
# ~ d   double  float   8
# ~ s   char[]  bytes   указывается явно
# ~ p   char[] — строка из Паскаля  bytes   указывается явно

# ~ >>> struct.pack("<h", 258)  # little-endian
# ~ b'\x02\x01'
# ~ >>> struct.pack(">h", 258)  # big-endian
# ~ b'\x01\x02'

#adequate printing of bytes
def my_str_of_bytes(bytes_str):
    try:
        return r"\x" + r"\x".join("{:02x}".format(c) for c in bytes_str)
    except:
        try:
            return r"\x" + "{:02x}".format(bytes_str)
        except:
            print('ERROR: input string is not "bytes" type')
            return ""

print("COMMANDS DESCRIPTION:")
print('"write command type -> " --- this means that you should write command from list below and press Enter for sending to PDA:')
print('"write reg" - if you want to write values into register:')
print('\t-> enter number of register')
print('\t-> enter 2bytes binary notation of nessasarry value')
print('"read reg" - if you want to read values from register:')
print('\t-enter number of register')
print('"wr reg" - if you want write values into register and read it from register.')
print('\t-> enter number of register')
print('\t-> enter 2bytes binary notation of nessasarry value')
print('"start meas" - start single cicle of measurement')
print('"stop meas" - forcedly stop measurement cicle')
print('"read data" - read data from PDA internal memory:')
print('\t-> enter start page of data')
print('\t-> enter stop page of data')
print("Notice: if entered command is wrong programm print ERROR")
print("Try to enjoy!!!")
print("")

# #def parse_daat(data_bytes):


def sender(socket):
    COMMANDS = {"write reg" : b'\x00', #0
              "start meas" : b'\x03', #1
                "read reg" : b'\x04', #2
               "stop meas" : b'\x05', #3
               "read data" : b'\x0d', #4
                  "wr reg" : b'\x0c',  #5
               "not exist" : b'\xff'  #5
                }

    KEYS = [*COMMANDS]

    #parsing of command, which you write in tirminal
    def parse_input(text):
        global PAGE_NUMB

        if not text in list(COMMANDS.keys()):
            print("ERROR: wrong command")
            return b'\xff'

        else:
            command_code = COMMANDS[text]
            print("Command code =", command_code.hex())
            if text == KEYS[0]:
                reg_numb = input("write rgister number (0-15) -> ")
                reg_numb = struct.pack(">b", int(reg_numb))
                reg_value = input("write register value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
                if len(reg_value.strip()) != 16:
                    print("ERROR: wrong length of bits sequence")
                    while len(reg_value.strip()) != 16:
                        reg_value = input("write register value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
                reg_value = struct.pack('>H', int(reg_value, 2))
                message = command_code + reg_numb + reg_value + b'\x00\x00'
                return message

            if text == KEYS[1]:
                message = command_code + b'\x00\x00\x00\x00\x00'
                return message

            if text == KEYS[2]:
                reg_numb = input("write register number (0-15) -> ")
                reg_numb = struct.pack(">b", int(reg_numb))
                message = command_code + reg_numb + b'\x00\x00\x00\x00'
                return message

            if text == KEYS[3]:
                message = command_code + b'\x00\x00\x00\x00\x00'
                return message

            if text == KEYS[4]:
                start_page = input("write data start page -> ")

                # #start_page = "0" #for read data test 1

                PAGE_NUMB = int(start_page)

                start_page = struct.pack('>H', int(start_page))

                stop_page = input("write data stop page -> ")

                # #stop_page = "63" #for read data test 1

                stop_page = struct.pack('>H', int(stop_page))

                message = bytes(command_code + b'\x00' + start_page + stop_page)
                return message

            if text == KEYS[5]:
                reg_numb = input("write register number (0-15) -> ")
                reg_numb = struct.pack(">b", int(reg_numb))
                reg_value = input("write register value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
                if len(reg_value.strip()) != 16:
                    print("ERROR: wrong length of bits sequence")
                    while len(reg_value.strip()) != 16:
                        reg_value = input("write register value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
                reg_value = struct.pack('>H', int(reg_value, 2))
                message = command_code + reg_numb + reg_value + b'\x00\x00'
                return message

            if text == KEYS[6]:
                message = command_code + b'\x00\x00\x00\x00\x00'
                return message

    while True:
        time.sleep(0.1)
        print("\nwrite command type -> ", end = "")
        tmp = input()
        message = parse_input(tmp.strip())

        # #message = parse_input("read data") #for read data test 1


        # ~ print("you message =", message)
        if message != b'\xff':               #prevent of sending message when command is wrong error
            time_mark = datetime.now()
            print(time_mark, "    send message:", ("{:<%d}" % LEN).format("") + my_str_of_bytes(message))
            sock.sendto(message, (UDP_IP, UDP_PORT))

# ~ sock.bind((UDP_IP, UDP_PORT))

BUFFER = bytes()
PAGE_NUMB = 0
print()

def receiver(socket):
    global PAGE_NUMB
    while True:
        data, addr = sock.recvfrom(2000)
        time_mark = datetime.now()

        prefix = ''
        meet = my_str_of_bytes(data)

        if my_str_of_bytes(data[0]) != r"\x" + "fd":

            BUFFER = bytes()

            if my_str_of_bytes(data[0]) == r"\x" + "10":   prefix = "ACK"
            elif my_str_of_bytes(data[0]) == r"\x" + "f4": prefix = "REG"
            elif my_str_of_bytes(data[0]) == r"\x" + "11": prefix = "CONF"
            else:                                          prefix = "????"

            prefix = ("{:<%d}" % LEN).format(prefix)
            print("%s received message: %s" % (time_mark, prefix + meet), flush = True)

        elif meet[:4] == r"\x" + "fd":

            hat = data[:10]
            body = data[10:]

            BUFFER += body
            # #meas_numb = struct.unpack(">B", hat[-1])[0]
            meas_numb = hat[-1]
            pack_numb = struct.unpack(">H", bytes(hat[3:5]))[0]
            init_page = struct.unpack(">H", bytes(hat[5:7]))[0]
            final_page = struct.unpack(">H", bytes(hat[7:9]))[0]

            if pack_numb == final_page:
                write_in_files(BUFFER)

            print("measurement number", meas_numb)
            print("pages", "[%d; %d]" % (init_page, final_page))
            print("package number", pack_numb)
            print("PAGE =", PAGE_NUMB, "BUFFER LEN =", len(BUFFER))

            print("types", type(data), type(body))

            # #test = struct.unpack(">c", bytes(body[0:1]))[0]
            PAGE_NUMB += 1

            meas_numb = hat[-1]
            prefix = "HAT"
            prefix = "{:<5}".format(prefix)
            print("%s received message: %s" % (time_mark, prefix + my_str_of_bytes(hat)), flush = True)
            # #print("measurement number", meas_numb, "body_len", len(body))
            # #print(my_str_of_bytes(body))
            # #body = data[10:]
            # #prefix = "DATA"
            # #prefix = "{:<5}".format(prefix)
            # #print("%s received message: %s" % (time_mark, prefix + meet), flush = True)


if __name__ == "__main__":
    UDP_IP = "172.16.1.166"
    UDP_PORT = 2195
    print("UDP target IP: %s" % UDP_IP)
    print("UDP target port: %s" % UDP_PORT)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    th_sender = Thread(target = sender, args = (sock,))
    th_sender.start()

    th_receiver = Thread(target = receiver, args = (sock,))
    th_receiver.start()

    # ~ th_receiver.join()
    # ~ th_receiver.join()


