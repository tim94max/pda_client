import struct
import numpy as np


# #byte_mess = b''
# #for i in list(range(24)):
    # #byte_mess += struct.pack("B", i)

tmp = []
k = 110
while sum(tmp) + k < 1024:
    tmp.append(k)
    if k >= 256:
        k -= 1
    else:
        k+=1
print(sum(tmp))
tmp.append(1024 - sum(tmp))
print(sum(tmp))
print(len(tmp))
print(tmp)

page = b''
for j in tmp:
    for i in list(range(j)):
        page += struct.pack("B", i)
print("page len", len(page))

# #page = b''
# #for i in list(range(16)):
    # #page += struct.pack("B", i)

#get any bytes buffer and parse it to list of turns
#   (turn - is 24 bytes of ADC bits, in order to convert it to int numbers
#   apply parse_one_turn function
#   return list of 24-bites turns and rest of buffer given after parsing
#
#   * you can put buffer any lenght into this function
#   if length < 24 function return "[], --you buffer--"
def parse_page_to_turns(buff):
    tmp = []
    k = 0
    while k < len(buff) // 24:
        tmp.append(buff[k * 24 : k * 24 + 24])
        k += 1
    rest = buff[- (len(buff) % 24):]
    return tmp, rest

#convert 24 bytes of one turn to int array
def parse_one_turn(byte_mess):
    if len(byte_mess) == 24:
        #converting bytes to bits string
        one_turn = ""
        for i in byte_mess:
            bin_not = "{:08b}".format(i)
            one_turn += bin_not

        #initialize 2D numpy array for parsing
        bits_arr = np.empty((12, 16), int)

        #parse whole bits string to 2-bites of one ADC bit from each 16 ADC channel (photodiodes)
        #    and stacking in list
        bits = []
        k = 0
        while k<=192:
            bits.append(one_turn[k:k+16])
            k+=16

        #filling 2D numpy array in order "as is"
        #    (stacking ADC bits)
        for i, val_i in enumerate(bits):
            for j, val_j in enumerate(val_i):
                bits_arr[i, j] = val_j

        #transpose array for workind not with every ADC bit
        #    but with ADC value
        #    and flipping (ex: 00010 -> 01000)
        #    because one turn begin with bit 0 and end with bit 11
        res = np.flip(np.transpose(bits_arr), axis = 1)

        #string bits numeration for number of ADC channel
        #    (this order defined by Karpov G.V.)
        D12 = res[0]
        D13 = res[1]
        D14 = res[2]
        D15 = res[3]
        D8 = res[4]
        D9 = res[5]
        D10 = res[6]
        D11 = res[7]
        D4 = res[8]
        D5 = res[9]
        D6 = res[10]
        D7 = res[11]
        D0 = res[12]
        D1 = res[13]
        D2 = res[14]
        D3 = res[15]

        #preparing each strings of ADC channel value for converting to int
        res_tot = np.array([D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D13, D14, D15])

        #converting -//- to int list
        data = []
        for i, val_i in enumerate(res_tot):
            tmp = "0000"
            for j, val_j in enumerate(res_tot[i]):
                tmp += str(val_j)
            byte = eval('0b' + tmp) - 2048 #(this order defined by Karpov G.V. for getting signed values)
            data.append(byte)
        return data
    else:
        print("ERROR: turn has length not 24 bytes")

def parse_tot(buff):
    turns = parse_page_to_turns(buff)
    res = []
    for i in turns[0]:
        res.append(parse_one_turn(i))
    return res

def write_in_files(buff):
    data = parse_tot(buff)
    with open("../data.csv", "w+") as f:
        for i in data:
            for j, val_j in enumerate(i):
                if j != 0:
                    f.write(", ")
                f.write("{:>5}".format("%d" % val_j))

            f.write("\n")



write_in_files(page)
