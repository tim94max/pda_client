import socket
import struct


    
# ~ x	байт набивки	нет значения	
# ~ c	char	bytes длины 1	1
# ~ b	signed char	integer	1
# ~ B	unsigned char	integer	1
# ~ ?	_Bool	bool	1
# ~ h	short	integer	2
# ~ H	unsigned short	integer	2
# ~ i	int	integer	4
# ~ I	unsigned int	integer	4
# ~ l	long	integer	4
# ~ L	unsigned long	integer	4
# ~ q	long long	integer	8
# ~ Q	unsigned long long	integer	8
# ~ n	ssize_t	integer	зависит
# ~ N	size_t	integer	зависит
# ~ e	«половинный float«	float	2
# ~ f	float	float	4
# ~ d	double	float	8
# ~ s	char[]	bytes	указывается явно
# ~ p	char[] — строка из Паскаля	bytes	указывается явно

# ~ >>> struct.pack("<h", 258)  # little-endian
# ~ b'\x02\x01'
# ~ >>> struct.pack(">h", 258)  # big-endian
# ~ b'\x01\x02'

COMMANDS = {"write reg" : b'\x00', #0
          "start meas" : b'\x03', #1
            "read reg" : b'\x04', #2
           "stop meas" : b'\x05', #3
           "read data" : b'\x0d', #4
              "wr reg" : b'\x0c'  #5
            }                        
# ~ COMMANDS = {"write reg" : struct.pack(">b", int(0)), #0
          # ~ "start meas" : struct.pack(">b", int(3)), #1
            # ~ "read reg" : struct.pack(">b", int(4)), #2
           # ~ "stop meas" : struct.pack(">b", int(5)), #3
           # ~ "read data" : struct.pack(">b", int(13)),#4
              # ~ "wr reg" : struct.pack(">b", int(12)) #5
            # ~ }
KEYS = [*COMMANDS]

print("COMMAND DESCRIPTION:")
print("---will be later---")

#adequate printing of bytes
def my_str_of_bytes(bytes_str):
    try:
        return r"\x" + r"\x".join("{:02x}".format(c) for c in bytes_str)
    except:
        print('ERROR: input string is not "bytes" type')
        return ""

#parsing of command, which is writed             
def parse_input(text):
    
    if not text in list(COMMANDS.keys()):
        print("ERROR: wrong command")
        return b'\x00' 
        
    else:
        command_code = COMMANDS[text]
        print("Command code =", command_code.hex()) 
        if text == KEYS[0]:
            reg_numb = input("write rgister number (0-15) -> ")
            reg_numb = struct.pack(">b", int(reg_numb))
            reg_value = input("write rgister value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
            reg_value = struct.pack('>h', int(reg_value, 2))
            message = command_code + reg_numb + reg_value + b'\x00\x00'
            return message
        
        if text == KEYS[1]:
            message = command_code + b'\x00\x00\x00\x00\x00'
            return message
            
        if text == KEYS[2]:
            reg_numb = input("write rgister number (0-15) -> ")
            reg_numb = struct.pack(">b", int(reg_numb))
            message = command_code + reg_numb + b'\x00\x00\x00\x00'
            return message
            
        if text == KEYS[3]:
            message = command_code + b'\x00\x00\x00\x00\x00'
            return message
            
        if text == KEYS[4]:
            start_page = input("write data start page -> ")
            start_page = struct.pack('>h', int(start_page))
            stop_page = input("write data start page -> ")
            stop_page = struct.pack('>h', int(stop_page))
            message = bytes(command_code + b'\x00' + start_page + stop_page)
            return message
            
        if text == KEYS[5]:
            reg_numb = input("write rgister number (0-15) -> ")
            reg_numb = struct.pack(">b", int(reg_numb))
            reg_value = input("write rgister value\n (in format 1111000011110000 - 2 bytes as 1 or 0) -> ")
            reg_value = struct.pack('>h', int(reg_value, 2))
            message = command_code + reg_numb + reg_value + b'\x00\x00'
            return message

UDP_IP = "172.16.1.166"
UDP_PORT = 2195
 
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# ~ sock.bind((UDP_IP, UDP_PORT))

MESSAGE = b"Hello, World!"
 
print("UDP target IP: %s" % UDP_IP)
print("UDP target port: %s" % UDP_PORT)
print("message: %s" % MESSAGE)

sock.sendto(MESSAGE, (UDP_IP, UDP_PORT))

while True:
    data, addr = sock.recvfrom(2000)
    print("received message: %s" % my_str_of_bytes(data))
    # ~ print(type(data.hex()))
    tmp = input("print command type -> ")
    message = parse_input(tmp)
    
    print("you message =", my_str_of_bytes(message))
    # ~ print("you message =", message)
    
    sock.sendto(message, (UDP_IP, UDP_PORT))
